[![pipeline status](https://gitlab.com/autonomic-roles/ufw/badges/master/pipeline.svg)](https://gitlab.com/autonomic-roles/ufw/commits/master)

# ufw

A role for managing firewalls with `ufw`.

# Hack On It

```bash
$ pip install -r requirements.txt
```

```bash
$ molecule test
```

# Use the Role

```yaml
  - hosts: server
    roles:
      - ufw
        ufw_allow_ports: [20, 80, 443]
```
